package com.example.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
@CrossOrigin(origins = "*")
@RestController
public class ImgList {

    @GetMapping("/files")
    public ResponseEntity<String> listFilesInDirectory() throws IOException {
        // 指定D盘根目录
        Path directory = Paths.get("/home/admin/ImgList");

        // 获取该目录及其子目录下的所有文件名
        List<Path> filePaths = Files.walk(directory)
                .filter(Files::isRegularFile) // 只选择文件而非目录
                .collect(Collectors.toList());

        // 提取文件名并转换为JSON格式
        List<String> filenames = filePaths.stream()
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());

        // 将文件名列表转换为JSON响应
        String jsonResult = "[" + filenames.stream()
                .map(filename -> "\"" + filename + "\"")
                .collect(Collectors.joining(",")) + "]";

        return ResponseEntity.ok(jsonResult);
    }
}